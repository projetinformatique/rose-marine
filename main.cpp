#include <iostream>
#include "snoopy.hpp"
using namespace std;

int main()
{
    Snoopy supersnoopy;

    cout << "x" << supersnoopy.getPos_x() << "y" << supersnoopy.getPos_y() << endl;
    //let's move
    supersnoopy.moving();

    cout << "x" << supersnoopy.getPos_x() << "y" << supersnoopy.getPos_y() << endl;

    return 0;
}
